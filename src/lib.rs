//! This crate contains some very commonly used and lightweight utility code.
//!
//! See the submodules for details.

#![deny(unsafe_code)]

pub mod procutils;
pub mod snafu_extensions;
pub mod threadutils;
pub mod timeout;
pub mod utils;

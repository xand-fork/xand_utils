use std::collections::HashMap;

pub fn split_to_hashmap(
    contents: &str,
    item_delim: &str,
    key_value_delim: &str,
) -> HashMap<String, String> {
    contents
        .trim()
        .split(item_delim)
        .map(|kv| kv.trim().split(key_value_delim))
        .filter_map(|mut kv| {
            if let (Some(key), Some(value)) = (kv.next(), kv.next()) {
                Some((key.trim().into(), value.trim().into()))
            } else {
                None
            }
        })
        .collect()
}
